# Star Wars Project

React Application with Star Wars Api.

## Live
[Netlify Live](https://star-wars-pub.netlify.app/)


## Aplication Examples (Desk and mobile Layout)
![](./readme_assets/desktop-preview.png)
![](./readme_assets/mobile-preview.jpg)

## Functional:
* Displaying cards with heroes with information about them.
* Adding and removing a card by one button.
* Small animation on card hover.
* Pagination, search and filtering by gender(A-Z, Z-A).
* Saving data about favorite characters in localstorage.
* Scroll up
* Responsive layout for mobile and desktop devices(not a tablet).

## Libraries and modules:
* React, redux, thunk, router-dom
* Axios (fetch data)
* propTypes (check type)
* clsx (customize styles)
* localtunnel (dev | test mobile response layout)
* prettier (code styles guide)
* stylelint and stylelint-order (stylelint and order for css)

##  Files and Directories:
```bash
|---public        ----/ Static project files
|---src
    |--- Components        --| Basic components from project
    |--- helpers           --| Helpers function
    |--- Layout            --| Layout for Project (Header, Footer, Main, etc)
    |--- Pages             --| Main page (Home, Favorite)
    |--- store             --| Redux
    |--- utils             --| Url constant
    |--- App.js            --| App wrapper and Routing
    |--- App.js            --| App wrapper styles on grid based
    |--- index.js          --| root file (Redux Provider)
    |--- index.css         --| root styles for all project
    |--- .gitignore        --| ignore folders
    |--.stylelintrc.json   --|config for styleling and styleorder

```
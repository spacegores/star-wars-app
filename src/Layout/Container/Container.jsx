import React from 'react';
import clsx from 'clsx'
import propTypes from 'prop-types';
import styles from './Container.module.css'

const Container = ({children, className}) => {
    return (
        <div className={clsx(styles.container, className)}>
            {children}
        </div>
    );
};

Container.propTypes = {
    children: propTypes.node.isRequired,
    className: propTypes.string,
}

export default Container;

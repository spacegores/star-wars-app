import React from 'react';
import propTypes from 'prop-types';
import styles from './Footer.module.css'

const Footer = ({children}) => {
    return (
        <footer className={styles.footer}>
            {
                children
            }
        </footer>
    );
};

Footer.propTypes = {
    children: propTypes.node.isRequired,
}

export default Footer;

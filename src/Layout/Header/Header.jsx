import React from 'react';
import propTypes from 'prop-types';
import styles from './Header.module.css'

const Header = ({children, ...props}) => {
    return (
        <header className={styles.header} {...props}>
            {children}
        </header>
    );
};

Header.propTypes = {
    children: propTypes.node.isRequired,
}


export default Header;

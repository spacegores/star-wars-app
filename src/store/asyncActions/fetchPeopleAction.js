import axios from "axios";
import {setIsFetching, setStarPeople, setTotalCount} from "../actions";
import { getId, getImg} from "../../helpers/helpers";

export const fetchPeoplesAction = (page = 1) => {
    return async (dispatch) => {

        dispatch(setIsFetching(true))

        const response = await axios.get(`https://swapi.dev/api/people/?page=${page}`)

        const pageTotalCount = response.data.count;

        const peoples = await Promise.all(
            response.data.results.map(async ({url, name, homeworld, gender}) => {

                const id = getId(url)
                const image = getImg(id)

                const { data: { name: planetName } } = await axios.get(homeworld);

                return {
                    id,
                    name,
                    image,
                    planetName,
                    gender
                }
            })
        );

        dispatch(setTotalCount(pageTotalCount))
        dispatch(setStarPeople(peoples))
    }
}
import axios from "axios";
import { setIsSearching } from "../actions";
import {getId, getImg} from "../../helpers/helpers";

export const searchPeopleActions = (search = null) => {
  return async (dispatch) => {
    const response = await axios.get(
      `https://swapi.dev/api/people/?search=${search}`
    );

    const peoples = await Promise.all(
        response.data.results.map(async ({url, name, homeworld, gender}) => {

          const id = getId(url)
          const image = getImg(id)

          const { data: { name: planetName } } = await axios.get(homeworld);

          return {
            id,
            name,
            image,
            planetName,
            gender
          }
        })
    );

    dispatch(setIsSearching(peoples));
  };
};

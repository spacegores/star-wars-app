import {
    DELETE_PEOPLE_FAVORITE,
    SET_IS_FETCHING, SET_IS_SEARCHING,
    SET_PEOPLE_FAVORITE,
    SET_STAR_PEOPLE,
    SET_TOTAL_COUNT
} from "../consts";


export const setToFavorite = (people) => ({type: SET_PEOPLE_FAVORITE, payload: people});

export const deleteFromFavorite = (people) => ({type: DELETE_PEOPLE_FAVORITE, payload: people})


export const setStarPeople = (peoples) => ({type: SET_STAR_PEOPLE, payload: peoples})

export const setTotalCount = (totalCount) => ({type: SET_TOTAL_COUNT, payload: totalCount})


export const setIsFetching = bool => ({type: SET_IS_FETCHING, payload: bool})

export const setIsSearching = searchingPeoples => ({type: SET_IS_SEARCHING, payload: searchingPeoples})
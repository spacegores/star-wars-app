import {combineReducers} from "redux";
import favoriteReducer from "./favoriteReducer";
import starPeopleReducer from "./starPeopleReducer";
import searchingPeopleReducer from "./starSearchingReducer";

export default combineReducers({
    favorite: favoriteReducer,
    people: starPeopleReducer,
    searching: searchingPeopleReducer,
})
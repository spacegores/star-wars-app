import {SET_IS_FETCHING, SET_STAR_PEOPLE, SET_TOTAL_COUNT} from "../consts";

const initialState = {
    starPeople: [],
    pageTotalCount: 0,
    isFetching: true
}

const starPeople = (state = initialState, action) => {

    switch (action.type) {
        case SET_TOTAL_COUNT:
            return {
                ...state,
                pageTotalCount: action.payload,
                isFetching: false
            }
        case SET_STAR_PEOPLE:
            return {
                ...state,
                starPeople: action.payload
            }
        case SET_IS_FETCHING:
            return {
                ...state,
                isFetching: action.payload
            }
        default: return state
    }
}

export default starPeople;
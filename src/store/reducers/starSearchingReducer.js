import {SET_IS_SEARCHING} from "../consts";

const initialState = {
    searchingPeoples: []
}

const searchingPeopleReducer = (state = initialState, action) => {

    switch (action.type) {
        case SET_IS_SEARCHING:
            return {
                ...state,
                searchingPeoples: action.payload,
                // isFetching: false
            }
        default: return state
    }
}

export default searchingPeopleReducer;
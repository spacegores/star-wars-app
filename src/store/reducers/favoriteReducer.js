import {DELETE_PEOPLE_FAVORITE, SET_PEOPLE_FAVORITE} from "../consts";

const initialState = {
    favoritePeople: []
}

const favoriteReducer = (state = initialState, action) => {

    switch (action.type) {
        case SET_PEOPLE_FAVORITE:
            return {
                ...state,
                favoritePeople: [...state.favoritePeople, action.payload]
            }
        case DELETE_PEOPLE_FAVORITE:
            return {
                ...state,
                favoritePeople: state.favoritePeople.filter(people => people.id !== action.payload)
            }
        default: return state
    }
}

export default favoriteReducer;
import {applyMiddleware, createStore} from "redux";
import cR from './reducers/index'
import {composeWithDevTools} from "redux-devtools-extension";
import thunk from "redux-thunk";

const saveToLocalStorage = (state) => {
    try {
        const serializedState = JSON.stringify(state)
        localStorage.setItem('state', serializedState)
    } catch (e) {
        console.warn(e)
    }
}

const loadFromLocalStorage = () => {
    try {
        const serializedState = localStorage.getItem('state');
        if(serializedState === null) return undefined;
        return JSON.parse(serializedState)
    } catch (e) {
        console.warn(e)
        return undefined;
    }
}

const store = createStore(cR, loadFromLocalStorage(), composeWithDevTools(applyMiddleware(thunk)));

store.subscribe(() => saveToLocalStorage(store.getState()))

export default store;
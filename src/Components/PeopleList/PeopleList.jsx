import React, { useEffect, useMemo, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchPeoplesAction } from "../../store/asyncActions/fetchPeopleAction";
import { deleteFromFavorite, setToFavorite } from "../../store/actions";
import { searchPeopleActions } from "../../store/asyncActions/searchPeoplesAction";
import Card from "../Card/Card";
import Pagination from "../Pagination/Pagination";
import Message from "../Message/Message";
import Search from "../Search/Search";
import Sorting from "../Sorting/Sorting";
import styles from "./PeopleList.module.css";

const PeopleList = () => {
  const dispatch = useDispatch();

  const [page, setPage] = useState(1);
  const [sort, setSort] = useState(false);
  const [peopleSorted, setPeopleSorted] = useState([]);

  const [searchValue, setSearchValue] = useState("");
  const isFirstRun = useRef(true);

  const isFetching = useSelector((state) => state.people.isFetching);
  const starWarsPeople = useSelector((state) => state.people.starPeople);
  const pageTotalCount = useSelector((state) => state.people.pageTotalCount);
  const favoritePeoples = useSelector((state) => state.favorite.favoritePeople);
  const searchingPeoples = useSelector(
    (state) => state.searching.searchingPeoples
  );

  useEffect(() => {
    setPeopleSorted([]);
    dispatch(searchPeopleActions(null));
    dispatch(fetchPeoplesAction(page));

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  useEffect(() => {
    if (isFirstRun.current) {
      isFirstRun.current = false;
      return;
    }
    dispatch(searchPeopleActions(searchValue));

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchValue]);

  useMemo(() => {
    const array = [...starWarsPeople].sort((a, b) => {
      return a.gender.localeCompare(b.gender) * (sort ? -1 : 1);
    });
    setPeopleSorted(array);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sort]);

  const toggleFavoritePeople = (id) => {
    const isHaveFavorite = favoritePeoples.some((people) => people.id === id);

    if (!isHaveFavorite) {
      if (searchingPeoples.length > 0) {
        const people = searchingPeoples.find((people) => people.id === id);
        dispatch(setToFavorite(people));
      } else {
        const people = starWarsPeople.find((people) => people.id === id);
        dispatch(setToFavorite(people));
      }
    } else {
      dispatch(deleteFromFavorite(id));
    }
  };

  const renderTemplate = () => {
    if (searchValue) {
      return searchingPeoples.length ? (
        searchingPeoples.map((people) => (
          <Card
            onClick={toggleFavoritePeople}
            isFavorite={people.isFavorite}
            key={people.id}
            name={people.name}
            img={people.image}
            id={people.id}
            planetName={people.planetName}
          />
        ))
      ) : (
        <Message image={"./joke.jpg"} text={"Упс! Героев нет!"} alt={'Нет героев'}/>
      );
    } else if (peopleSorted.length > 0) {
      return peopleSorted.map((people) => (
        <Card
          onClick={toggleFavoritePeople}
          isFavorite={people.isFavorite}
          key={people.id}
          name={people.name}
          img={people.image}
          id={people.id}
          planetName={people.planetName}
        />
      ));
    } else if (starWarsPeople) {
      return starWarsPeople.map((people) => (
        <Card
          onClick={toggleFavoritePeople}
          isFavorite={people.isFavorite}
          key={people.id}
          name={people.name}
          img={people.image}
          id={people.id}
          planetName={people.planetName}
        />
      ));
    } else {
      return <div>Нет данных</div>;
    }
  };

  return (
    <div className={styles["people-list"]}>
      {isFetching ? (
        <Message
          className={styles["people-loading"]}
          image={"./loading.jpg"}
          text={"Терпение! Джедаи ужинают сейчас. Хе-хе..."}
          alt={"Картинка загрузки"}
        />
      ) : (
        <>
          <div className={styles.toolbar}>
            <Pagination
              data={starWarsPeople}
              pageCount={pageTotalCount}
              pageLimit={5}
              dataLimit={10}
              setPage={setPage}
              page={page}
            ></Pagination>
            <Search setSearchValue={setSearchValue} searchValue={searchValue} />
            <Sorting sort={sort} setSort={setSort} />
          </div>
          <div className={styles["people-list__wrapper"]}>
            {renderTemplate()}
          </div>
        </>
      )}
    </div>
  );
};

export default PeopleList;

import React from 'react';
import propTypes from 'prop-types';
import Image from "../Image/Image";
import styles from './Message.module.css'

const Message = ({image, text, alt, ...props}) => {
    return (
        <div className={styles.message} {...props}>
            <Image src={image} alt={alt}/>
            <h2>{text}</h2>
        </div>
    );
};

Message.propTypes = {
    image: propTypes.string.isRequired,
    text: propTypes.string.isRequired,
}

export default Message;

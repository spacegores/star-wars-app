import React from 'react';
import styles from './HeartIcon.module.css'
import propTypes from 'prop-types'


const HeartIcon = ({width, height, fill}) => {
    return (
        <svg className={styles['heart-icon']} xmlns="http://www.w3.org/2000/svg" width={width} height={height} viewBox="0 0 24 24" fill={fill}><path d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z"/></svg>

    );
};

HeartIcon.propTypes = {
    width: propTypes.number.isRequired,
    height: propTypes.number.isRequired,
    fill: propTypes.string.isRequired,
}

export default HeartIcon;

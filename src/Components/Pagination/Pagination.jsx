import React, { useState } from "react";
import clsx from "clsx";
import propTypes from 'prop-types';
import Button from "../Button/Button";
import styles from "./Pagination.module.css";

function Pagination({ pageCount, dataLimit, setPage, page }) {
  const pages = Math.round(pageCount / dataLimit);
  const [currentPage, setCurrentPage] = useState(page);

  function goToNextPage() {
    setCurrentPage((page) => page + 1);
    setPage((page) => page + 1);
  }

  function goToPreviousPage() {
    setCurrentPage((page) => page - 1);
    setPage((page) => page - 1);
  }

  function changePage(event) {

    const pageNumber = Number(event.target.textContent);
    setCurrentPage(pageNumber);
    setPage(pageNumber);
  }

  const getPaginationGroup = () => {
    let pagesCount = [];

    for (let i = 1; i < pages + 1; i++) {
      pagesCount.push(i);
    }
    return pagesCount;
  };

  return (
    <div>
      {
        !pageCount ? null :
            <div className={styles.pagination}>
              <Button
                  onClick={goToPreviousPage}
                  disabled={currentPage === 1}
                  className={clsx(styles["paginate-button"], {
                    [styles["button-disabled"]]: currentPage === 1,
                  })}
              >
                Назад
              </Button>
              {getPaginationGroup().map((item, index) => (
                  <Button
                      key={index}
                      onClick={changePage}
                      className={clsx(styles['button-page'], {
                        [styles['button-active']]: currentPage === item
                      })}
                  >
                    <span>{item}</span>
                  </Button>
              ))}
              <Button
                  className={clsx(styles["paginate-button"], {
                    [styles["button-disabled"]]: currentPage === pages,
                  })}
                  onClick={goToNextPage}
                  disabled={currentPage === pages}
              >
                Вперед
              </Button>
            </div>
      }
    </div>
  );
}

Pagination.propTypes = {
  pageCount: propTypes.number.isRequired,
  dataLimit: propTypes.number.isRequired,
  setPage: propTypes.func.isRequired,
  page: propTypes.number.isRequired

}

export default Pagination;

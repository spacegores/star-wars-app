import React from 'react';
import Image from "../Image/Image";
import propTypes from "prop-types";

const ResumeLink = ({href}) => {
    return (
        <a target="_blank" rel="noreferrer" href={href}>
            <Image width={40} height={40} src={'./hh.png'} alt={'Ссылка на резюме'}/>
        </a>
    );
};

ResumeLink.propTypes = {
    href: propTypes.string.isRequired,
}

export default ResumeLink;

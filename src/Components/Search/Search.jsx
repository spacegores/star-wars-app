import React from "react";
import propTypes from 'prop-types';
import styles from "./Search.module.css";

const Search = ({searchValue, setSearchValue}) => {
  return (
        <input
            className={styles.search}
            placeholder={"Искать"}
            type="text"
            value={searchValue}
            onChange={(e) => setSearchValue(e.target.value)}
        />
  );
};

Search.propTypes = {
    searchValue: propTypes.string,
    setSearchValue: propTypes.func.isRequired,
}

export default Search;

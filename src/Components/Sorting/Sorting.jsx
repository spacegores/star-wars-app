import React, { useState } from "react";
import propTypes from "prop-types";
import styles from "./Sorting.module.css";

const Sorting = ({ sort, setSort }) => {
  const [active, setActive] = useState(false);

  const onClickHandler = () => {
    setActive((active) => !active);
  };

  const sorting = () => {
    setSort(!sort);
  };

  return (
    <div onClick={onClickHandler} className={styles.sortingWrapper}>
      <ul className={styles.sorting}>
        Сортировать по:
        <li onClick={sorting} className={active ? styles.param : styles.hide}>
          По полу
        </li>
      </ul>
    </div>
  );
};

Sorting.propTypes = {
    sort: propTypes.bool.isRequired,
    setSort: propTypes.func.isRequired
}

export default Sorting;

import React from 'react';
import propTypes from 'prop-types';
import BabyYodaIcon from "../Icons/BabyYodaIcon";
import styles from './FavoriteYodaCount.module.css'

const FavoriteYodaCount = ({width, height, count}) => {
    return (
        <div className={styles['loading-count']}>
            <BabyYodaIcon width={width} height={height}/>
            <span className={styles.count}>{count}</span>
        </div>
    );
};

FavoriteYodaCount.propTypes = {
    width: propTypes.number.isRequired,
    height: propTypes.number.isRequired,
    count: propTypes.number.isRequired
}

export default FavoriteYodaCount;

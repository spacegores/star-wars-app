import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import Button from "../Button/Button";
import HeartIcon from "../Icons/HeartIcon";
import propTypes from "prop-types";
import styles from "./Card.module.css";

const Card = ({ name, id, img, onClick, planetName, ...props }) => {
  const isFavoritePage = props.location?.pathname === "/favorite";

  const [active, setActive] = useState(false || isFavoritePage);

  const favoritePeoples = useSelector((state) => state.favorite.favoritePeople);

  const isHaveFavorite = favoritePeoples.some((people) => people.id === id);

  useEffect(() => {
    if (isHaveFavorite) {
      setActive(true);
    }
  }, [isHaveFavorite]);

  const onClickHandler = () => {
    setActive((active) => !active);
    onClick(id);
  };

  return (
    <div style={{ backgroundImage: `url(${img})` }} className={styles.card}>
      <div className={styles["card-overlay"]}></div>
      <div className={styles.border}>
        <h2>{name}</h2>
        <h3 style={{ color: "white" }}>{planetName}</h3>
        <div>
          <Button
            className={styles["card-button"]}
            onClick={() => onClickHandler()}
          >
            <HeartIcon height={25} width={25} fill={active ? "red" : "white"} />
            <span className={styles["button-text"]}>
              {isFavoritePage ? "Удалить" : "Добавить"}
            </span>
          </Button>
        </div>
      </div>
    </div>
  );
};

Card.propTypes = {
  name: propTypes.string.isRequired,
  id: propTypes.number.isRequired,
  img: propTypes.string.isRequired,
  onClick: propTypes.func.isRequired,
  planetName: propTypes.string.isRequired,
};

export default Card;

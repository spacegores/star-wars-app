import React from 'react';
import propTypes from 'prop-types';
import styles from './Image.module.css'

const Image = ({width, height, alt, src,  ...props}) => {
    return (
        <img className={styles.image} width={width} height={height} src={src} alt={src} {...props}/>
    );
};

Image.propTypes = {
    width: propTypes.number,
    height: propTypes.oneOfType([
        propTypes.string,
        propTypes.number
    ]),
    alt: propTypes.string.isRequired,
    src: propTypes.string.isRequired,
}

export default Image;

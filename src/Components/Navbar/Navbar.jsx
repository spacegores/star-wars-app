import React from 'react';
import {Link} from 'react-router-dom'
import {useSelector} from "react-redux";
import FavoriteYodaCount from "../FavoriteYodaCount/FavoriteYodaCount";
import styles from './Navbar.module.css'

const Navbar = () => {
    
    const favoritePeoples = useSelector((state) => state.favorite.favoritePeople);

    const favoriteCount = favoritePeoples.length;

    return (
        <nav className={styles.navbar}>
            <ul>
                <li>
                    <Link to={'/'}>Главная</Link>
                </li>
                <li>
                    <Link className={styles['link-loading-count']} to={'/favorite'}>
                        <span>Любимые герои</span>
                        <FavoriteYodaCount height={28} width={28} count={favoriteCount}/>
                    </Link>

                </li>
            </ul>
        </nav>
    );
};

export default Navbar;

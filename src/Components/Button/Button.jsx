import React from 'react';
import clsx from 'clsx';
import propTypes from 'prop-types'
import styles from './Button.module.css'

const Button = ({children, onClick, className,  ...props}) => {
    return (
        <button onClick={onClick} className={clsx(styles.button, className)} {...props}>
            {children}
        </button>
    );
};

Button.propTypes = {
    children: propTypes.node.isRequired,
    onClick: propTypes.func,
    className: propTypes.string,
}

export default Button;

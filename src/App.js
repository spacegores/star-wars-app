import React from 'react';
import {BrowserRouter, Switch, Route, Redirect} from 'react-router-dom'
import Home from "./Pages/Home/Home";
import Favorite from "./Pages/Favorite/Favorite";
import styles from './App.module.css'

const App = () => {

    return (
        <div className={styles.app}>
            <BrowserRouter>
                    <Switch>
                        <Route exact path="/" component={Home}/>
                        <Route exact path="/favorite" component={Favorite}/>
                        <Redirect to="/"/>
                    </Switch>
                </BrowserRouter>
        </div>
    );
};

export default App;

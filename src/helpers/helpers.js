import { IMAGE_URL } from "../utils/consts";

export const getId = (url) => {
  return +/\d+/.exec(url);
};

export const getImg = (id) => `${IMAGE_URL}${id}.jpg`;
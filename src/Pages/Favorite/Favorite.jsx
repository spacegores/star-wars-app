import React from "react";
import { useLocation } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { deleteFromFavorite } from "../../store/actions";
import Header from "../../Layout/Header/Header";
import Container from "../../Layout/Container/Container";
import Navbar from "../../Components/Navbar/Navbar";
import Logo from "../../Components/Logo/Logo";
import Main from "../../Layout/Main/Main";
import Card from "../../Components/Card/Card";
import Message from "../../Components/Message/Message";
import Image from "../../Components/Image/Image";
import Footer from "../../Layout/Footer/Footer";
import styles from "./Favorite.module.css";
import ScrollToTop from "react-scroll-to-top";

const Favorite = () => {
  const dispatch = useDispatch();

  const location = useLocation();

  const favoritePeoples = useSelector((state) => state.favorite.favoritePeople);

  const deleteFavoritePeople = (id) => {
    dispatch(deleteFromFavorite(id));
  };
  return (
    <>
      <Header>
        <Container className={styles["header-container"]}>
          <Logo width={150} height={150} />
          <Navbar />
        </Container>
      </Header>
      <Main>
        {favoritePeoples.length < 1 ? (
          <Message
            image={"./joke.jpg"}
            text={"Падаван юный в избранное геров не добавил"}
            alt={'Изображение "Не добавлено в избранное"'}
          />
        ) : (
          <Container className={styles["people-list"]}>
            {favoritePeoples.map((people) => (
              <Card
                onClick={deleteFavoritePeople}
                key={people.name}
                id={people.id}
                name={people.name}
                url={people.url}
                img={people.image}
                location={location}
                planetName={people.planetName}
              />
            ))}
          </Container>
        )}
      </Main>
      <Footer>
        <Container className={styles["footer-container"]}>
          <Logo width={150} height={150} />
          <a href="https://lipetsk.hh.ru/resume/bf28bf32ff08d16e9a0039ed1f44455a4e5558">
            <Image
              width={40}
              height={40}
              src={"./hh.png"}
              alt={"Изображение резюме"}
            />
          </a>
        </Container>
      </Footer>
      <div className="scroll-to-top-wrapper">
        <ScrollToTop smooth />
      </div>
    </>
  );
};

export default Favorite;

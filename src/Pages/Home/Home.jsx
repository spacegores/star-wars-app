import React from 'react';
import Navbar from "../../Components/Navbar/Navbar";
import Header from "../../Layout/Header/Header";
import Container from "../../Layout/Container/Container";
import Logo from "../../Components/Logo/Logo";
import Main from "../../Layout/Main/Main";
import PeopleList from "../../Components/PeopleList/PeopleList";
import Footer from "../../Layout/Footer/Footer";
import ResumeLink from "../../Components/ResumeLink/ResumeLink";
import styles from './Home.module.css'
import ScrollToTop from "react-scroll-to-top";

const Home = () => {

    return (
        <>
            <Header>
                <Container className={styles['header-container']}>
                    <Logo width={150} height={150}/>
                    <Navbar/>
                </Container>
            </Header>
            <Main>
                <Container className={styles['people-list']}>
                   <PeopleList/>
                </Container>
            </Main>
            <Footer>
                <Container className={styles['footer-container']}>
                    <Logo width={150} height={150}/>
                    <ResumeLink href={'https://lipetsk.hh.ru/resume/bf28bf32ff08d16e9a0039ed1f44455a4e5558'}/>
                </Container>
            </Footer>
            <div className="scroll-to-top-wrapper">
                <ScrollToTop smooth/>
            </div>
        </>
    );
};

export default Home;
